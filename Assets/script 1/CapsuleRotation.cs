using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    [SerializeField]
    private Vector3 axes;
    public float scaleUnits;
    // Update is called once per frame
    void Update()
    {
        axes = MovimientoTransform.ClampVector3(axes);
        transform.localScale += axes * (scaleUnits * Time.deltaTime);

    }
}

