using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public float speed = 4f;
    public float angularSpeed = 30f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.up * (-angularSpeed * Time.deltaTime));
        }
        else if (Input.GetKey(KeyKode.D))
        {
            transform.Rotate(Vector3.up * (angularSpeed * Time.deltaTime));
        }
        if (Input.GetKey(KeyKode.W))
        {
            transform.Translate(Vector3.forward * (Time.deltaTime * Speed));
        }
        else if (Input.GetKey(KeyKode.S))
        {
            transform.Translate(Vector3.back * (Time.deltaTime * Speed));
        }
    }
}
